package guarderia;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class Guarderia implements Serializable {
    private ArrayList<Usuario> usuarios;
    private ArrayList<Zona> zonas;
    private ArrayList<Empleado> empleados;
    
    public Guarderia() {
         usuarios = new ArrayList<>();
         zonas = new ArrayList<>();
         empleados = new ArrayList<>();
    }
    
    public Guarderia deSerializar(String a) throws IOException, ClassNotFoundException {
        FileInputStream f = new FileInputStream(a);
        ObjectInputStream o = new ObjectInputStream(f);
        Guarderia g = (Guarderia) o.readObject();
        o.close();
        f.close();
        return g;
    }
    
    public void serializar(String a)throws IOException{
        FileOutputStream f = new FileOutputStream(a);
        ObjectOutputStream o = new ObjectOutputStream(f);
        o.writeObject(this);
        o.close();
        f.close();
    }
    
    public ArrayList<Usuario> getUsuarios(){
        return usuarios;
    }

        
    public void setUsuarios(ArrayList<Usuario> usuarios){
        this.usuarios = usuarios;
    }
    
    
    public Usuario buscarUsuario(String datos){
        int i = 0;
        boolean encontrado = false;
        Usuario u = null;
        
        while(i<usuarios.size() && !encontrado) {
            u = usuarios.get(i);
            if(datos.equals(u.getUsuario() + ":" + u.getPassword())) {
                encontrado = true;
            } else {
                i++;
            }
        }
        if (!encontrado){
            return null;
        } else {
            return u;
        }
    }
       
    
    public Usuario buscarSocio(String dato){
        int i =0;
        boolean encontrado = false;
        Usuario s = null;
        
        while(i<usuarios.size() && !encontrado) {
            s = usuarios.get(i);
            if(dato.equals(s.getUsuario())) {
                encontrado = true;
            } else {
                i++;
            }
        }
        if (!encontrado){
            return null;
        } else {
            return s;
        }
    }
    
    
    public Empleado buscarEmpleado(String nombre) {
        for (Empleado empleado : empleados) {
            if (empleado.getNombre().equalsIgnoreCase(nombre)) {
                return empleado;
            }
        }
        return null;
    }
    
    public ArrayList<Zona> getZonas() {
        return zonas;
    }

    public void setZonas(ArrayList<Zona> zonas) {
        this.zonas = zonas;
    }
    
    
}
    

