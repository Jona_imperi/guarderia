package guarderia;

import java.io.IOException;

public class Inicio {
    
    public void ejecutar(){
        Guarderia guarderia = new Guarderia();

        boolean seguir;
        try{
            guarderia = guarderia.deSerializar("coches.txt");
            seguir = true;
        } catch (IOException | ClassNotFoundException e) {

            String u = EntradaSalida.leerString("Arranque incial del sistema.\n Administrador, ingrese su nombre de usuario ");
            if(u.equals("")){
                throw new NullPointerException("ERROR, el usuario no puede ser nulo");
            }

            String p = EntradaSalida.leerString("Ingrese su password: ");
            if(p.equals("")){
                throw new NullPointerException("ERROR, la password no puede ser nula");
            }

            guarderia.getUsuarios().add(new Administrador(u, p));

            try{
                guarderia.serializar("coches.txt");
                EntradaSalida.mostrarString("Administrador registrado. Debe reiniciarse el sistema.");
            } catch (IOException ex){}
            seguir = false;
        }


        while(seguir){
            String u = EntradaSalida.leerString("Ingrese el usuario: ");
            String p = EntradaSalida.leerString("Ingrese la password ");

            Usuario us = guarderia.buscarUsuario(u + ":" + p);

            if(us == null){
                EntradaSalida.mostrarString("No se ha encontrado el usuario o la password es incorrecta");
            } else {
                seguir = us.proceder(guarderia);
            }
        }  
    }
   
    
}
