package guarderia;

import java.io.IOException;
import java.io.Serializable;


public class Administrador extends Usuario implements Serializable{
    private AltaBaja ab = new AltaBaja();
    private OperacionConGarage opGarage = new OperacionConGarage();
    
    public Administrador(String u, String p){
        setUsuario(u);
        setPassword(p);
    }
    
    @Override
    public void mostrar(){
        System.out.println("---------------------------");
        System.out.println("    FICHA ADMINISTRADOR ");
        System.out.println("---------------------------");
        System.out.println("Administrador: " + this.getUsuario());
        System.out.println("Password: " + this.getPassword());
        System.out.println("          ---");
    }

    @Override
    public boolean proceder(Guarderia guarderia) {
        char op;
        boolean seguir = true;
              
        do{ 
            op = EntradaSalida.leerChar("""
                                        
                                        OPCIONES DE ADMINISTRADOR 
                                        [1] Dar alta socio 
                                        [2] Dar alta empleado 
                                        [3] Dar alta administrador 
                                        [4] Listar usuarios 
                                        [5] Dar baja usuario
                                        [6] Gestionar vehiculos y garages
                                        [7] Consultar disponibilidad garage 
                                        [8] Asignar empleado a garage 
                                        [9] Salir del menu 
                                        [0] Salir del sistema 
                                        """);
            switch (op) {
                
                case '1':
                    ab.altaSocio(guarderia);
                    break;
                case '2':
                    ab.altaEmpleado(guarderia);
                    break;
                case '3':
                    ab.altaAdministrador(guarderia);
                    break;
                case '4':
                    ab.listarUsuarios(guarderia);
                    break;
                case '5':
                    ab.bajaUsuario(guarderia);
                    break;
                case '6':
                    opGarage.gestionarVehiculo(guarderia);
                    break;
                case '7':
                   opGarage.consultarCapacidadGarages(guarderia);
                    break;
                case '8':
                    opGarage.asignarEmpleadoAGarage(guarderia);
                    break;
                case '9':
                    seguir = true;
                    break;
                case '0':
                    seguir = false;
                    break;
                default:
                    EntradaSalida.mostrarString("Error, opcion invalida");
                    op = '*';                    
            }
            if (op >= '1' && op <='8'){
                try {
                    guarderia.serializar("coches.txt");
                } catch (IOException e){
                    e.printStackTrace();
                }
            }       
        } while(op!='9' && op!='0');
        
        return seguir;

    }


}
    