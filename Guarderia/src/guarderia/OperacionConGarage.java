package guarderia;

import java.io.Serializable;

public class OperacionConGarage implements Serializable {
    
    public void gestionarVehiculo(Guarderia guarderia){
        char o = EntradaSalida.leerChar("""
                                        [1] Registrar vehiculo de Socio 
                                        [2] Asignacion de vehiculo a Garage 
                                        [3] Compra de garage
                                        """);
        
        switch (o) {
            case '1':
                registrarVehiculo(guarderia);
                break;
            case '2':
                //asignarVehiculoAGarage(guarderia);
                break;
            case '3':
                comprarGarage(guarderia);
                break;
            default:
                EntradaSalida.mostrarString("Opción no válida.");
                break;
        }
        
    }
    
    public void registrarVehiculo(Guarderia guarderia){
        String matricula = EntradaSalida.leerString("Ingrese el matricula  del vehiculo: ");
        String nombre = EntradaSalida.leerString("Ingrese el nombre  del vehiculo: ");
        String tipo = EntradaSalida.leerTipo();
        String dimensiones = EntradaSalida.leerString("Ingrese dimensiones  del vehiculo: ");
        Vehiculo v = new Vehiculo(matricula, nombre, tipo, dimensiones);
        String usuarioSocio = EntradaSalida.leerString("Ingrese usuario a que pertenece el vehiculo: ");
        Usuario s = guarderia.buscarSocio(usuarioSocio);
        
        if (s != null) {
        s.agregarVehiculo(v);
        System.out.println("Vehículo registrado exitosamente.");
        } else {
        System.out.println("El usuario no fue encontrado.");
    }
    }
    
    
    
    
    public Zona buscarZonaPorTipo(Guarderia guarderia, String tipo) {
        for (Zona zona : guarderia.getZonas()) {
            if (zona.getClass().getSimpleName().equalsIgnoreCase("Zona" + tipo)) {
                return zona;
            }
        }
        return null;
    }
    
    
     public void comprarGarage(Guarderia guarderia) {
        String usuarioSocio = EntradaSalida.leerString("Ingrese el usuario del socio: ");
        Socio socio = (Socio) guarderia.buscarSocio(usuarioSocio);

        if (socio == null) {
            System.out.println("El socio no fue encontrado.");
            return;
        }

        String tipoZona = EntradaSalida.leerString("Ingrese el tipo de zona (Motorhome, CasasRodantes, Trailers, Caravana): ");
        int numeroGarage = EntradaSalida.leerInt("Ingrese el número del garage a comprar: ");

        Zona zona = buscarZonaPorTipo(guarderia, tipoZona);
        if (zona == null) {
            System.out.println("No existe una zona para el tipo especificado: " + tipoZona);
            return;
        }

        for (Garage garage : zona.getGarages()) {
            if (garage.getNumero() == numeroGarage) {
                if (garage.estaComprado()) {
                    System.out.println("El garage ya ha sido comprado.");
                } else {
                    garage.comprar(socio);
                    System.out.println("El garage ha sido comprado por " + socio.getUsuario());
                }
                return;
            }
        }
        System.out.println("No se encontró el garage con el número especificado.");
    }

    public void consultarCapacidadGarages(Guarderia guarderia) {
         ZonaMotorhome a = new ZonaMotorhome(); 
         a.consultarDisponibilidad();
         ZonaCaravana b = new ZonaCaravana(); 
         b.consultarDisponibilidad();
         ZonaCasaRodante c = new ZonaCasaRodante(); 
         c.consultarDisponibilidad();
         ZonaTrailer d = new ZonaTrailer(); 
         d.consultarDisponibilidad();
        
      }
    
    
    
    public void asignarEmpleadoAGarage(Guarderia guarderia) {
        String nombreEmpleado = EntradaSalida.leerString("Ingrese el nombre del empleado: ");
        Empleado empleado = guarderia.buscarEmpleado(nombreEmpleado);

        if (empleado == null) {
            System.out.println("El empleado no fue encontrado.");
            return;
        }

        String tipoZona = EntradaSalida.leerString("Ingrese el tipo de zona (Motorhome, CasasRodantes, Trailers, Caravana): ");
        int numeroGarage = EntradaSalida.leerInt("Ingrese el número del garage a asignar: ");

        Zona zona = buscarZonaPorTipo(guarderia, tipoZona);
        if (zona == null) {
            System.out.println("No existe una zona para el tipo especificado: " + tipoZona);
            return;
        }

        for (Garage garage : zona.getGarages()) {
            if (garage.getNumero() == numeroGarage) {
                garage.asignarEmpleado(empleado);
                System.out.println("El empleado " + empleado.getNombre() + " ha sido asignado al garage " + numeroGarage);
                return;
            }
        }
        System.out.println("No se encontró el garage con el número especificado.");
    }
}
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

