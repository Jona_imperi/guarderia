package guarderia;

public abstract class Zona {
   
    public abstract Garage[] getGarages();
    
    public abstract Garage obtenerGarageDisponible();

    public abstract void consultarDisponibilidad();

    
}
