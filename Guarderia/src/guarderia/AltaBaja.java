package guarderia;

import java.io.Serializable;
import java.util.ArrayList;

public class AltaBaja implements Serializable{
    
     
    public void altaSocio(Guarderia guarderia) {
        String socio = EntradaSalida.leerString("Ingrese nombre de usuario: ");
            if(socio.equals("")){
                EntradaSalida.mostrarString("ERROR, el usuario no puede ser nulo");
            } else {
                String pass = EntradaSalida.leerString("Ingrese la password: ");
                
                if(pass.equals("")){
                    EntradaSalida.mostrarString("ERROR, la password no puede ser nula");
                } else {
                    Usuario u = guarderia.buscarUsuario(socio + ":" + pass);
                    
                    if (u != null){
                        EntradaSalida.mostrarString("El usuario ya figura en el sistema");
                    } else {
                        String DNI = EntradaSalida.leerString("Ingresar DNI: ");
                        String nombre = EntradaSalida.leerString("Ingresar nombre: ");
                        String telefono = EntradaSalida.leerString("Ingresar telefono: ");
                        String direccion = EntradaSalida.leerString("Ingresar direccion: ");
                        u = new Socio(socio, pass, DNI, nombre, telefono, direccion);
                        guarderia.getUsuarios().add(u);
                        EntradaSalida.mostrarString("Se ha incorporado un nuevo socio al sistema");
                    }
                }
            }
    }        
            


    public void altaEmpleado(Guarderia guarderia) {
        String empleado = EntradaSalida.leerString("Ingrese nombre de usuario: ");
            if(empleado.equals("")){
                EntradaSalida.mostrarString("ERROR, el usuario no puede ser nulo");
            } else {
                String pass = EntradaSalida.leerString("Ingrese la password: ");
                
                if(pass.equals("")){
                    EntradaSalida.mostrarString("ERROR, la password no puede ser nula");
                } else {
                    Usuario e = guarderia.buscarUsuario(empleado + ":" + pass);
                    
                    if (e != null){
                        EntradaSalida.mostrarString("El usuario ya figura en el sistema");
                    } else {
                        String codigo = EntradaSalida.leerString("Ingresar codigo: ");
                        String nombre = EntradaSalida.leerString("Ingresar nombre: ");
                        String telefono = EntradaSalida.leerString("Ingresar telefono: ");
                        String direccion = EntradaSalida.leerString("Ingresar direccion: ");
                        String especialidad = EntradaSalida.leerString("Ingresar especialidad ");
                        e = new Empleado(empleado, pass, codigo, nombre, telefono, direccion, especialidad);
                        guarderia.getUsuarios().add(e);
                        EntradaSalida.mostrarString("\tSe ha incorporado un empleado al sistema");
                    }
                }
            }
    }
    
    public void altaAdministrador(Guarderia guarderia) {
        String admin = EntradaSalida.leerString("Ingrese nombre de usuario");
            if(admin.equals("")){
                EntradaSalida.mostrarString("ERROR, el usuario no puede ser nulo");
            } else {
                String pass = EntradaSalida.leerString("Ingrese la password: ");
                
                if(pass.equals("")){
                    EntradaSalida.mostrarString("ERROR, la password no puede ser nula");
                } else {
                    Usuario a = guarderia.buscarUsuario(admin + ":" + pass);
                    
                    if (a != null){
                        EntradaSalida.mostrarString("El usuario ya figura en el sistema");
                    } else {
                        a = new Administrador(admin, pass);
                        guarderia.getUsuarios().add(a);
                        EntradaSalida.mostrarString("\tSe ha incorporado un administrador al sistema");
                    }
                }
            }
    }
    
    public void listarUsuarios(Guarderia guarderia) {     
        System.out.println("-USUARIOS-\n");
        ArrayList<Usuario> listaUser = guarderia.getUsuarios();
        for(int i=0; i<listaUser.size(); i++){
            listaUser.get(i).mostrar();
        }    
    }
    
    
    public void bajaUsuario(Guarderia guarderia) {
        String nombre = EntradaSalida.leerString("Ingrese nombre de usuario a eliminar: ");
        if (nombre.equals("")) {
            EntradaSalida.mostrarString("ERROR, el nombre de usuario no puede ser nulo");
        } else {
            ArrayList<Usuario> usuarios = guarderia.getUsuarios();
            boolean encontrado = false;
            for (int i = 0; i < usuarios.size(); i++) {
                if (usuarios.get(i).getUsuario().equals(nombre)) {
                    usuarios.remove(i);
                    EntradaSalida.mostrarString("El usuario ha sido eliminado del sistema");
                    encontrado = true;
                    break;
                }
            }
            if (!encontrado) {
                EntradaSalida.mostrarString("ERROR, el usuario no se encuentra en el sistema");
            }
        }
    }
}
    


