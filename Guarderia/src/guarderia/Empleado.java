package guarderia;

import java.io.Serializable;

public class Empleado extends Usuario implements Serializable {
    
    private String codigo, nombre, direccion, telefono, especialidad;
    
    
    
    public Empleado(String u, String p, String codigo, String nombre, String telefono, String direccion, String especialidad){
        setUsuario(u);
        setPassword(p);
        this.codigo = codigo;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.especialidad = especialidad;
    }

    
    @Override
    public boolean proceder(Guarderia guarderia) {
        return false;
    }

    @Override
    public void mostrar() {
        System.out.println("-------------------------");
        System.out.println("    FICHA EMPLEADO ");
        System.out.println("-------------------------");
        System.out.println("Usuario: " + this.getUsuario());
        System.out.println("Password: " + this.getPassword());
        System.out.println("Codigo:" + codigo);
        System.out.println("Nombre: " + nombre);
        System.out.println("Direccion: " + direccion);
        System.out.println("Telefono: " + telefono);
        System.out.println("Especialidad: " + especialidad);
        System.out.println("        ---");
    }
    
    public String getNombre(){
        return nombre;
    }
}
