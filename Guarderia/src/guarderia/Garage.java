package guarderia;

public class Garage {
    private int numero;
    private boolean disponible;
    private boolean comprado;
    private Socio propietario;
    private Empleado empleadoAsignado;
    
    public Garage(int numero) {
        this.numero = numero;
        this.disponible = true;
        this.comprado = false;
        this.propietario = null;
        this.empleadoAsignado = null;
    }

    public boolean estaDisponible() {
        return disponible && !comprado;
    }

    public void ocupar() {
        this.disponible = false;
    }

    public void liberar() {
        if (!comprado) {
            this.disponible = true;
        }
    }

    public boolean estaComprado() {
        return comprado;
    }

    public Socio getPropietario() {
        return propietario;
    }

    public void comprar(Socio socio) {
        this.comprado = true;
        this.propietario = socio;
        this.disponible = false;
    }

    public int getNumero() {
        return numero;
    }
    
     public Empleado getEmpleadoAsignado() {
        return empleadoAsignado;
    }

    public void asignarEmpleado(Empleado empleado) {
        this.empleadoAsignado = empleado;
    }
}