package guarderia;

public class ZonaMotorhome extends Zona {
    private static final int CANT_GARAGES = 20;
    private Garage[] garages;

    public ZonaMotorhome() {
        
        this.garages = new Garage[CANT_GARAGES];
        for (int i = 0; i < CANT_GARAGES; i++) {
            this.garages[i] = new Garage(i + 1);
        }
    }

    @Override
    public Garage[] getGarages() {
        return garages;
    }

    @Override
    public Garage obtenerGarageDisponible() {
        for (Garage garage : garages) {
            if (garage.estaDisponible()) {
                return garage;
            }
        }
        return null;
    }
    
    @Override
    public void consultarDisponibilidad() {
        int disponibles = 0;
        for (Garage garage : garages) {
            if (garage.estaDisponible()) {
                disponibles++;
            }
        }
        System.out.println("Capacidad zona Motorhome: " + (CANT_GARAGES-disponibles) + "/" + CANT_GARAGES);
    }
}