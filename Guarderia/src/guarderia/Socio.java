package guarderia;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class Socio extends Usuario implements Serializable {
    
    private String DNI, nombre, direccion, telefono;
    private ArrayList<Vehiculo> vehiculos;
    private LocalDateTime fechaAlta;
        
    public Socio(String u, String p, String DNI, String nombre, String telefono, String direccion){
        setUsuario(u);
        setPassword(p);
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.DNI = DNI;
        vehiculos = new ArrayList<Vehiculo>();
        fechaAlta = LocalDateTime.now();
    }
    
    @Override
    public boolean proceder(Guarderia guarderia) {
        char op;
        boolean seguir = true;
              
        do{ 
            op = EntradaSalida.leerChar("""
                                        
                                             OPCIONES DE SOCIO 
                                        [1] Consultar datos personales 
                                        [2] Ver estado garage 
                                        [3] Salir del menu 
                                        [0] Salir del sistema 
                                        """);
            switch (op) {
                case '1':
                    mostrar();
                    break;
                case '2':
                    System.out.println("nada");
                    break;
                case '3':
                    seguir = true;
                    break;
                case '0':
                    seguir = false;
                    break;
                default:
                    EntradaSalida.mostrarString("Error, opcion invalida");
                    op = '*';                    
            }
            if (op >= '1' && op <='2'){
                try {
                    guarderia.serializar("coches.txt");
                } catch (IOException e){}
            }       
        } while(op!='3' && op!='0');
        
        return seguir;
    }

    @Override
    public void mostrar() {
        System.out.println("---------------------");
        System.out.println("    FICHA SOCIO ");
        System.out.println("---------------------");
        System.out.println("Usuario " + this.getUsuario());
        System.out.println("Password: " + this.getPassword());
        System.out.println("DNI:    " + DNI);
        System.out.println("Nombre:   " + nombre);
        System.out.println("Direccion:  " + direccion);
        System.out.println("Telefono: " + telefono);
        DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        System.out.println("Fecha de Ingreso: " + this.fechaAlta.format(formato));
        System.out.println("        ---");
    }
    
    public void agregarVehiculo(Vehiculo vehiculo) {
        this.vehiculos.add(vehiculo);
    }
    
    public ArrayList<Vehiculo> getVehiculos() {
        return vehiculos;
    }
}
